#!/bin/bash
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


function help_text() {
    echo "usage: workon_plugin [-h] REMOTE_NAME PLUGIN_NAME [PLUGIN_VER] [PLUGIN_DEP] ..."
    exit
}

[ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "-help" ] && {
    help_text
}

PWD=$(pwd)


function workon_plugin() {
    local target_branch=$1
    local target_plugin=$2
    [ -z "$target_plugin" ] || {
        echo "switch to $target_plugin"
        git checkout $target_plugin
        mkdir -p templates/build
        rm -rf templates/build/*
        for i in $(ls templates); do
            if [[ "$i" != "README.md" && "$i" != "build" ]];then
                cp -rf templates/$i templates/build
            fi
        done
    }
    echo "switch to $target_branch"
    git checkout $target_branch
    for i in $(ls templates); do
        if [[ "$i" != "README.md" && "$i" != "build" ]];then
            cp -rf templates/build/* templates/$i
        fi
    done
}

function main() {
    local target_branch="$1"
    local target_ver="$2"
    local target_remote="$3"
    local target_plugin="$4"
    local branch=$(git branch -a| grep remotes | grep "$target_remote" | grep "$target_ver" | grep "$target_branch" | awk -F'/' '{print $3}')
    [ -z "$branch" ] && {
        return
    }
    local plugin=$(git branch -a| grep remotes | grep "$target_remote" | grep "$target_plugin" | awk -F'/' '{print $3}')
    workon_plugin "$branch" "$plugin"
}

remote=$1
branch=$2
ver=$3
[ -z "$ver" ] && {
    ver="0.1.0"
}
plugin=$4
[ -z "$plugin" ] && {
    plugin="tool"
}
[ "$remote" != "" ] && [ "$branch" != "" ] && {
    main "$branch" "$ver" "$remote" "$plugin"
} || {
    help_text
}

