#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import sys
import json
import datetime
import logging
import traceback

from sqlalchemy import create_engine, Column, Integer, String, DateTime, Float, Boolean, Text, and_, or_, distinct, func
from flask import Flask, request, make_response, send_from_directory
from flask_restful import Api, Resource, reqparse

if sys.version_info > (3, 0):
    unicode = str


class OutException(Exception):
    pass


def _check_request(parser, arg_name, arg_type, default=None, need_exist=True, location=('form', 'json', 'args'),
                   check_in=None, check_without=None,
                   checker=None):
    parser.add_argument(arg_name, type=arg_type, location=location)
    args = parser.parse_args()
    if hasattr(request, 'sub_dict') and isinstance(getattr(request, 'sub_dict'), dict):
        _arg_val = getattr(request, 'sub_dict').get(arg_name, default)
    else:
        _arg_val = args.get(arg_name, default)
    if need_exist:
        if not _arg_val:
            raise OutException(u'Argument {} not found'.format(arg_name))
    else:
        if not _arg_val and default:
            _arg_val = default
    if _arg_val:
        if check_in and isinstance(check_in, (list, tuple)) and _arg_val not in check_in:
            raise OutException(u'Argument {} is available in {}'.format(arg_name, tuple(check_in)))
        if check_without and isinstance(check_without, (list, tuple)) and _arg_val in check_without:
            raise OutException(u'Argument {} is not allowed in {}'.format(arg_name, tuple(check_without)))
    if _arg_val:
        if arg_type == unicode:
            _arg_val = _arg_val.encode('utf-8')
        import sys
        if sys.version_info > (3, 0):
            _arg_val = _arg_val.decode()
        if isinstance(checker, Checker) and not checker.exist:
            checker.exist = True
    return _arg_val


def _put_response(data=None, total=None, error=None, mime_type=None, status_code=None, headers=None):
    _data = dict()
    _data['status'] = status_code
    if not headers:
        headers = dict()
    _data['headers'] = headers
    if not mime_type:
        _data['mime'] = 'application/json'
    else:
        _data['mime'] = mime_type
    if error is not None:
        if not status_code:
            _data['status'] = 700
        _data['error_msg'] = str(error)
    elif data is not None:
        if not status_code:
            _data['status'] = 200
        _data['data'] = data
        if total is not None:
            _data['total'] = total
    return _data


# 是否跨域
_check_origin = True
# True为过滤所有异常, False只过滤OutException
_check_exception = False
# 是否重定向code到status
_check_code = False


def _check_response(pass_data=None, 
                    check_origin=_check_origin, 
                    check_exception=_check_exception, 
                    check_code=_check_code):
    import functools

    def _filter_session(func):
        @functools.wraps(func)
        def __filter_session(*args, **kwargs):
            def _make_response(d):
                if 'mime' in d:
                    _mime_type = d.pop('mime')
                else:
                    _mime_type = 'application/json'
                if 'headers' in d:
                    _headers = d.pop('headers')
                else:
                    _headers = {}
                if 'status' in d:
                    _status = d['status']
                else:
                    _status = 200
                    d['status'] = _status

                if 'error_msg' in d:
                    d['status'] = 700
                if 'code' not in d:
                    d['code'] = 0

                if check_code and d['code'] != 0:
                    d['status'] = d['code']

                try:
                    if _mime_type == 'application/json':
                        d = json.dumps(d)
                    else:
                        if 'data' in d:
                            d = d['data']
                except:
                    logging.error(traceback.print_exc())
                _d = make_response(d, _status)
                for i, j in _headers.items():
                    _d.headers[i] = j
                _d.headers['Content-Type'] = _mime_type
                if check_origin:
                    _d.headers['Access-Control-Allow-Origin'] = '*'
                    _d.headers['Access-Control-Allow-Method'] = 'POST,GET,PUT,DELETE,PATCH,OPTIONS'
                    _d.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
                return _d

            try:
                ret = func(*args, **kwargs)
                logging.debug(u'Response:{0}'.format(ret))
                if isinstance(ret, dict):
                    return _make_response(ret)
                else:
                    return ret
            except Exception as e:
                logging.error(traceback.print_exc())
                _data = _put_response(error=e)
                logging.debug(u'EResponse:{0}'.format(_data))
                _error = _data['error_msg'].split('[')[0]
                if _error:
                    if 'child watchers are only available on the default loop' in str(_error):
                        if pass_data:
                            return _make_response(pass_data)
                    else:
                        _data['error_msg'] = _error
                if not check_exception:
                    if isinstance(e, OutException):
                        _data['status'] = 200
                        if hasattr(e, 'code'):
                            _data['code'] = getattr(e, 'code')
                else:
                    if isinstance(e, Exception):
                        _data['status'] = 200
                        if hasattr(e, 'code'):
                            _data['code'] = getattr(e, 'code')
                return _make_response(_data)

        return __filter_session

    return _filter_session


class Checker(object):
    def __init__(self):
        self.exist = False


def _request_parser():
    return reqparse.RequestParser(trim=True)


def _uuid():
    import uuid
    return str(uuid.uuid4()).replace('-', '')


def _like(d):
    return '%{}%'.format(d)


def _datetime(d):
    if d:
        return d.strftime('%Y-%m-%d %H:%M:%S')
    else:
        return ''


def _trans_datetime(d):
    try:
        return datetime.datetime.fromtimestamp(int(d))
    except:
        return d


def try_times(count):
    _count = {'count': count}

    def _try_times(func):
        def __try_times(*args, **kwargs):
            _ret = None
            for i in range(_count.get('count', 1)):
                _ret = func(*args, **kwargs)
                if _ret:
                    return _ret
            return _ret
        return __try_times
    return _try_times


def _filter_query(p, q, filter_dict, filter_model):
    for i, j in filter_dict.items():
        _i_name = _check_request(p, i, unicode, need_exist=False)
        if _i_name:
            _rule = or_(*[getattr(filter_model, j).like(_like(_name))
                          for _name in _i_name.split(',')])
            q = q.filter(_rule)
    return q


def _filter_page(p, q, offset=None, size=None, page=None):
    if not offset:
        offset = _check_request(p, 'offset', str, need_exist=False)
    if offset:
        q = q.offset(offset)
    if not size:
        size = _check_request(p, 'size', str, need_exist=False)
    if size:
        q = q.limit(size)
        if not page:
            page = _check_request(p, 'page', str, need_exist=False)
            if page:
                q = q.offset((int(page) - 1) * int(size))
    return q


def check_api_middleware(api_name, method_name, err_msg):
    import functools

    def _api_middleware(func):
        @functools.wraps(func)
        def __api_middleware(*args, **kwargs):
            _self = args[0]

            def _get_api():
                if hasattr(_self, '__interface__'):
                    try:
                        _api = getattr(_self, '__interface__').get(api_name)
                        _ret = getattr(_api, method_name)(*args, **kwargs)
                    except:
                        import traceback
                        raise Exception(err_msg)
                    if not _ret:
                        raise Exception(err_msg)
                else:
                    raise Exception(err_msg)
                return _ret if _ret else {}

            _r = _get_api()
            kwargs.update(**_r)
            return func(*args, **kwargs)
        return __api_middleware
    return _api_middleware


def call_api_function(handler, api_name, method_name, *args, **kwargs):
    if hasattr(handler, '__interface__'):
        try:
            _api = getattr(handler, '__interface__').get(api_name)
            _ret = getattr(_api, method_name)(*args, **kwargs)
            return _ret, True
        except:
            import traceback
            raise Exception(traceback.print_exc())
    return None, False


def set_api_args(key, value=None, reset=False):
    if not hasattr(request, 'sub_dict'):
        setattr(request, 'sub_dict', dict())
    if reset:
        setattr(request, 'sub_dict', dict())
    getattr(request, 'sub_dict')[key] = value


def call_api_off():
    if hasattr(request, 'sub_dict'):
        delattr(request, 'sub_dict')
