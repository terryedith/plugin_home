#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


def desencrypt(data, key=b'z7KXXtJ^', iv=b'8o-TLksM'):
    from pyDes import des, triple_des, ECB, PAD_PKCS5
    import base64
    k = des(key, ECB, iv, pad=None, padmode=PAD_PKCS5)
    encrystr = k.encrypt(data)
    return base64.b64encode(encrystr)


def encrypt_des(data, key=b'z7KXXtJ^', iv=b'8o-TLksM'):
    from pyDes import des, triple_des, CBC, PAD_PKCS5
    import base64
    k = des(key[:8], CBC, iv[:8], pad=None, padmode=PAD_PKCS5)
    d = k.encrypt(data.encode())
    d = base64.encodestring(d)
    return d.decode()


def decrypt_des(data, key=b'z7KXXtJ^', iv=b'8o-TLksM'):
    from pyDes import des, triple_des, CBC, PAD_PKCS5
    import base64
    k = des(key[:8], CBC, iv[:8], pad=None, padmode=PAD_PKCS5)
    data = base64.decodestring(data.encode())
    d = k.decrypt(data)
    return d.decode()


def encrypt_aes(time, base_key, data):
    '''
    AES的ECB模式加密方法
    :param time: 10位时间戳
    :param base_key: 15位基础密钥
    :param data:被加密字符串（明文）
    :return:密文
    '''
    from Crypto.Cipher import AES
    import base64

    BLOCK_SIZE = 16  # Bytes
    pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * \
                    chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
    unpad = lambda s: s[:-ord(s[len(s) - 1:])]

    key = (base_key + time[-1]).encode('utf8')
    # 字符串补位
    data = pad(data)
    cipher = AES.new(key, AES.MODE_ECB)
    # 加密后得到的是bytes类型的数据，使用Base64进行编码,返回byte字符串
    result = cipher.encrypt(data.encode())
    encodestrs = base64.b64encode(result)
    enctext = encodestrs.decode('utf8')
    return enctext


def decrypt_aes(time, base_key, data):
    '''
    :param time: 10位时间戳
    :param base_key: 15位基础密钥
    :param data: 加密后的数据（密文）
    :return:明文
    '''
    from Crypto.Cipher import AES
    import base64

    BLOCK_SIZE = 16  # Bytes
    pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * \
                    chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
    unpad = lambda s: s[:-ord(s[len(s) - 1:])]

    key = (base_key + time[-1]).encode('utf8')
    data = base64.b64decode(data)
    cipher = AES.new(key, AES.MODE_ECB)

    # 去补位
    text_decrypted = unpad(cipher.decrypt(data))
    text_decrypted = text_decrypted.decode('utf8')
    return text_decrypted
