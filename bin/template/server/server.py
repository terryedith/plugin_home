#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import time
import datetime
import tornado.gen
import tornado.ioloop
import tornado.wsgi
import tornado.web
from tornado.ioloop import IOLoop
from functools import wraps


_ROUTE = {}


@tornado.gen.coroutine
def async_sleep(seconds):
    yield tornado.gen.Task(IOLoop.instance().add_timeout, time.time() + seconds)


def call_later(delay=0):
    def wrap_loop(func):
        @wraps(func)
        def wrap_func(*args, **kwargs):
            return IOLoop.instance().call_later(delay, func, *args, **kwargs)

        return wrap_func

    return wrap_loop


def call_event(delta=60):
    _delta = delta * 1000
    _args = {'args': []}

    def wrap_loop(func):
        @wraps(func)
        @tornado.gen.coroutine
        def wrap_func(*args, **kwargs):
            ret = None
            if not _args['args']:
                _args['args'] = args
            try:
                ret = func(*_args['args'], **kwargs)
            except Exception as e:
                pass

            IOLoop.instance().add_timeout(
                datetime.timedelta(milliseconds=_delta), wrap_func)
            return ret

        return wrap_func

    return wrap_loop


def initialize(self, **kwargs):
    for k, v in kwargs.items():
        setattr(self, k, v()) if callable(v) else setattr(self, k, v)


tornado.web.RequestHandler.initialize = initialize


class Application(tornado.web.Application):

    def __init__(self, *handlers, **settings):
        _session_settings = {
            'driver': 'memory',
            'driver_settings': dict(host=self),
            'force_persistence': True,
            'sid_name': 'torndsessionID',
            'session_lifetime': 1800
        }
        settings.update(**dict(session=_session_settings))
        super(Application, self).__init__(*handlers, **settings)


_APP = Application


def make_app(route=None):
    _target_route = route if route else _ROUTE
    return _APP(_target_route)


def make_route(**routes):
    global _ROUTE
    _target_routes = routes
    _target_routes = [(r'{0}'.format(i), j)
                      for i, j in _target_routes.items()]
    _ROUTE = _target_routes
    return _ROUTE


def classprobe(entry_method, **kwargs):
    def _singleton(cls, *args, **kw):
        instances = {}
        if hasattr(cls, entry_method) and callable(getattr(cls, entry_method)):
            getattr(cls, entry_method)(**kwargs)

        def __singleton():
            if cls not in instances:
                instances[cls] = cls(*args, **kw)
            return instances[cls]

        return __singleton

    return _singleton


if __name__ == "__main__":
    from concurrent.futures import ThreadPoolExecutor
    import json

    @classprobe('run_plugins')
    class MainHandler(tornado.web.RequestHandler):
        executor = ThreadPoolExecutor(100)

        @classmethod
        @call_later(3)
        def run_plugins(cls):
            pass

    class TestHandler(tornado.web.RequestHandler):

        def get(self):
            print(json.loads(self.request.body))
            self.write('get')

        def post(self):
            print(json.loads(self.request.body))
            self.write('post')

    r = dict()
    r[r'/'] = MainHandler
    r[r'/test'] = TestHandler
    route = make_route(**r)
    app = make_app(route)
    app.listen(8889, '127.0.0.1')
    tornado.ioloop.IOLoop.current().start()

