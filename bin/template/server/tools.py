#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015-2018  Terry Xi
# All Rights Reserved.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


def check_api_middleware(api_name, method_name, err_msg):
    import functools

    def _api_middleware(func):
        @functools.wraps(func)
        def __api_middleware(*args, **kwargs):
            _self = args[0]

            def _get_api():
                if hasattr(_self, '__interface__'):
                    try:
                        _api = getattr(_self, '__interface__').get(api_name)
                        _ret = getattr(_api, method_name)(*args, **kwargs)
                    except:
                        import traceback
                        raise Exception(err_msg)
                    if not _ret:
                        raise Exception(err_msg)
                else:
                    raise Exception(err_msg)
                return _ret if _ret else {}

            _r = _get_api()
            kwargs.update(**_r)
            return func(*args, **kwargs)
        return __api_middleware
    return _api_middleware
