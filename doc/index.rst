插件使用说明
======


目录:

.. toctree::
    :maxdepth: 2

    src/changelog.rst
    src/intr.rst
    src/app.rst
    src/install_cloud.rst
    src/deploy_handbook.rst
    src/deploy_jenkins.rst
    src/test_data.rst
    src/plugin_api.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
