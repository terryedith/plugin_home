# 实时流数据分析数据分块脚本文档


维护人 | 版本号 | 描述 | 日期
--- | --- | --- | ---
彭楚伟 | 0.1 | 初版 | 2019-08-27

## 1.1 触发参数传递
* 触发传递参数：file_str

* 远端收到file_str为一个字符串，远端url配置参数名为`request_url`
* file_str示例:d99fad96a5d0a7cb2eb41341ea556093_t3600_1566886079:{analysis1},{analysis2},
* ':'分割0位,'_' 分割此字符串，得到0位为设备类型md5，1位为数据切割分块规则，2位为起始时间戳；

对应的远端文件为：

* d99fad96a5d0a7cb2eb41341ea556093_t3600_1566886079, 
* d99fad96a5d0a7cb2eb41341ea556093_t3600_1566886079.timeinfo, 
* d99fad96a5d0a7cb2eb41341ea556093.outline, 
* d99fad96a5d0a7cb2eb41341ea556093.outline_timeinfo
* d99fad96a5d0a7cb2eb41341ea556093.conf

timeinfo 文件包含对应数据包起始与终止时间，格式为
```
start:1566886079
stop:1566889679
```

outline_timeinfo 文件存储离线数据最后一条的时间戳，格式为
```
outline_endtime:1566889679
```
outline 文件为离线数据文件包
conf 文件为对应类型设备配置文件，可直接转json

## 1.2 ini文件说明
app:main 配置说明

* data_dir ：数据集存储路径，支持相对与绝对路径
* time_field ：数据时间戳字段key
* time_interval ：轮巡时长
* process_pid ：异步任务pid

设备类型 section 格式说明：
* identify识别配置；
* collect_method采集配置：只允许t或者n开头，接数字，表示时间多少秒或多少条数据；
* normal_tag正常数据范围配置列表，必须同时满足列表中的条件value为列表的表示该值只允许
为列表中的值，字符串开头结尾只允许英文([和)}字符,中间必须是两个-隔开的数字，表示取值范围，如 
"(0.1-20)"，只有一段有限制，数字不写，如 "(-20)"

* analysis_name1 = 数据分析事件别名
* analysis_size1 = 数据分析事件尺度


## 1.3 ini配置示例
```
[app:main]
app_name = realtime_analysis
app_db = app.db
data_dir = dataset
time_field = timestamp
time_interval = 10
process_pid = 1111
tips = 格式说明：identify识别配置；collect_method采集配置：只允许t或者n开头，接数字，表示时间多少秒或多少条数据；normal_tag正常数据范围配置列表，必须同时满足列表中的条件value为列表的表示该值只允许为列表中的值，字符串开头结尾只允许英文([和)}字符,中间必须是两个-隔开的数字，表示取值范围，如 "(0.1-20)"，只有一段有限制，数字不写，如 "(-20)"

[pressure]
identify = {"type": "pressure"}
collect_method = t3600
normal_tag = [{"me": "(0.1-20],(25-40)"}, {"me": ['a', 'b']}]
analysis_name1 = half_hour
analysis_size1 = 1800
analysis_name2 = ten_min_all
analysis_size2 = 600
analysis_name3 = ten_min_some
analysis_size3 = 600
```



