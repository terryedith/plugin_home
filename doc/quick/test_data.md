# 模拟数据脚本文档


维护人 | 版本号 | 描述 | 日期
--- | --- | --- | ---
彭楚伟 | 0.1 | 初版 | 2019-07-08

## 1 使用方法

* 脚本名称：`uwsgi_plugins/templates/collect_test_data/app.py`位于分支`plugin_collect_test_data_0.1.0`下

* 该脚本自动读取app.ini文件中`[plugin_status]`下配置项开启状态，状态为`on`的选项生成数据为打开状态，并按照
`[time]`下`time_interval`的值为生成时间间隔生成模拟数据，单位：s

* 直接调用app.py文件，返回数据。脚本调用遵循插件调用一般规则！

## 2 开发者说明

### 2.1 新增模拟数据集
基本功能已经实现，需要新增迷你数据集请在类`Application`下新增方法开发。
* 注意状态不同，数据结构也不同！

示例：
```
def humiture(self):
        # 'JX_KY_HUMITURE': 'serialNumber',
        time_now = int(time.time())
        random_time = self.random_number(1500000000, time_now, 2)
        voltageStatus = self.random_enum([0, 1])
        resp_dict = {
            "_did": 'deviceMac',
            "_dtype": 'HUMITURE',
            "_dmanu": 'KY',
            "_dver": '0.1.0',
            "_name": 'collect_humiture',
            "_version": '0.2.0',

            "modelName": "AN-303",
            "serialNumber": "ffffff100000937f",
            "nickName": "温湿度传感器",
            "voltage": self.random_number(3, 5, 1),
            "temperature": self.random_number(0, 35, 1),
            "humidity": self.random_number(0, 100, 1),
            "timestamp": random_time,
            "voltageStatus": voltageStatus,
        }
        # 状态信息
        humiture_status_dict = {
            "_status": 'status',
            "_state": "0",
            "projectId": 1,
            "createTime": self.time_new(random_time),
            "dataType": self.random_enum([1, 2, 3]),
            "id": self.random_number(1000, 10000, 0),
        }
        # 报警信息
        humiture_report_dict = {
            "modelId": "teststr",  # 传感器型号ID
            "modelName": "teststr",  # 传感器型号
            "_status": 'report',

            "_err_code": "0",  # 0,100
            "_err_level": "",  # 1,5
            "_err_details": "",

        }
        if voltageStatus == 0:
            resp_dict.update(humiture_status_dict)
        elif voltageStatus == 1:
            resp_dict.update(humiture_report_dict)
        resp_dict.update(self.base_dict)
        try:
            did_info = self.get_did('humiture')
            resp_dict.update(did_info)
        except:
            pass
        self.add_report(resp_dict)
        return resp_dict
```

### 2.2 逻辑实现流程

本脚本通过类`Application`下`read_config`方法读取app.ini中配置，并以这些配置调用对应方法生成数据集，
脚本调用通过通用 init，start等方法获取返回数据

