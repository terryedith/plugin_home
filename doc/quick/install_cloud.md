# 全节点自动化部署脚本文档


维护人 | 版本号 | 描述 | 日期
--- | --- | --- | ---
彭楚伟 | 0.1 | 初版 | 2019-07-05

## 1.1 依赖
* 脚本依赖
```
本脚本依赖环境
系统：ubuntu16.04
基础依赖包：docker-compose，docker，unzip
```

脚本名称：`uwsgi_plugins/templates/tools/install_cloud`，位于 `tool` 分支下

## 1.2 节点
* 该脚本共包含7个节点的自动化部署

节点名列表；
```
deploy_database_node     数据服务数据库节点
deploy_center_node       数据中心节点
deploy_process_node      数据处理节点
deploy_storage_node      数据存储节点
deploy_bus_node          数据总线节点
deploy_collect_node      数据采集节点
add_node                 新增节点
```


## 2 脚本使用
* 依赖环境：ubuntu16.04，docker，docker-compose

### 2.1 帮助命令

帮助命令可以让你看到相关的参数及其配置需要
* 在使用部署脚本部署数据处理节点、数据存储节点、数据总线节点前，请先设置配置文件 main 的 mysql_password 配置选项
```
[main]
;development, production
run_method = development
mysql_password = 12345678  # 必须按照需求设置！
```

1、脚本帮助命令：`./install_cloud -h`

```
root@wise-Vostro-3668:/opt/app/uwsgi_plugins/templates/tools# ./install_cloud -h
usage: install_cloud [-h]
                     {deploy_cache_node,deploy_database_node,deploy_center_node,deploy_process_node,deploy_storage_node,deploy_bus_node,deploy_collect_node,add_node}
                     ...

positional arguments:
  {deploy_cache_node,deploy_database_node,deploy_center_node,deploy_process_node,deploy_storage_node,deploy_bus_node,deploy_collect_node,add_node}
    deploy_cache_node   数据采集缓存节点
    deploy_database_node
                        数据服务数据库节点
    deploy_center_node  数据中心节点
    deploy_process_node
                        数据处理节点
    deploy_storage_node
                        数据存储节点
    deploy_bus_node     数据总线节点
    deploy_collect_node
                        数据采集节点
    add_node            新增节点

optional arguments:
  -h, --help            show this help message and exit
```
2、节点安装帮助命令：`./install_cloud {节点名} -h`
```
root@wise-Vostro-3668:/opt/app/uwsgi_plugins/templates/tools# ./install_cloud deploy_cache_node -h
usage: install_cloud deploy_cache_node [-h]
                                       [--database-install DATABASE_INSTALL]
                                       [--mongodb-port MONGODB_PORT]
                                       [--mongodb-dir MONGODB_DIR]
                                       [--mysql-port MYSQL_PORT]
                                       [--mysql-dir MYSQL_DIR]
                                       [--mysql-passwd MYSQL_PASSWD]
                                       [--postgre-port POSTGRE_PORT]
                                       [--postgre-passwd POSTGRE_PASSWD]

optional arguments:
  -h, --help            show this help message and exit
  --database-install DATABASE_INSTALL
                        要部署的缓存节点数据库，默认7,代表全部，可选‘mongodb-1’，‘mysql-2’，‘postgres
                        ql-4’,多选使用数字代号相加，如3代表mongodb和mysql
  --mongodb-port MONGODB_PORT
                        mongodb数据库外部映射端口，默认27010
  --mongodb-dir MONGODB_DIR
                        mongodb数据库外部映射存储位置，注意路径！，默认$PWD/db
  --mysql-port MYSQL_PORT
                        mysql数据库外部映射端口，默认23300
  --mysql-dir MYSQL_DIR
                        mysql数据库外部映射存储位置,存入指定目录下级，注意路径！，默认$PWD
  --mysql-passwd MYSQL_PASSWD
                        mysql数据库密码，默认12345678
  --postgre-port POSTGRE_PORT
                        mysql数据库外部映射端口，默认25430
  --postgre-passwd POSTGRE_PASSWD
                        postgre数据库密码，默认12345678
```
### 2.2 运行命令

* 没有需要特殊配置的选项，可不需要添加关键字参数，使用默认配置完成安装

`./install_cloud {节点名}`

* 有需要自定义的配置项，根据各节点安装帮助信息（2.1 2）设置相关自定义配置关键字参数！

`./install_cloud {节点名} --{参数名} {参数信息} --{参数名} {参数信息} ……`

### 2.3 新增一个附加的节点（主线主备模式）
* 主备模式在同一机器布置适用此法
* 此法部署新节点加入了必须指定的四个新的参数

```
  --addition-node ADDITION_NODE
                        如为附加节点，则需要指定
  --addition-server-port ADDITION_SERVER_PORT
                        附加节点服务端口，此端口必须在开放端口列表中，且不可与其他端口冲突，虽然本程序有机制跳过已用端口，但仍可能需
                        要多次调整，示例：WizCollect-14010,WizSpider-14011
  --addition-port-num ADDITION_PORT_NUM
                        附加节点开放端口数量，默认10
  --addition-port-limit ADDITION_PORT_LIMIT
                        附加节点开放端口限定范围，两组数字，必须指定，"-"隔开,例：2000-2100
```

`./install_cloud deploy_bus_node --addition-node {参数信息(自定义节点名信息)} --addition-server-port {参数信息} --addition-port-num 10 --addition-port-limit 14000-14200`

示例：
`./install_cloud deploy_bus_node --addition-node add_node`
其它参数有默认

* **注意,新增节点的服务端口号必须在开放端口中，且指定的端口不可以已被占用**

## 3 脚本开发者文档
### 3.1 参数

```
# 数据采集缓存节点
command = self.command('deploy_cache_node', help_text=u'数据采集缓存节点')
command.install_argument(['--database-install'], 'database_install', default='7',
                         help_text=u'要部署的缓存节点数据库，默认7,代表全部，可选‘mongodb-1’，‘mysql-2’，‘postgresql-4’,'
                                   u'多选使用数字代号相加，如3代表mongodb和mysql')
```

### 3.2 入口
### 3.2.1 主程序入口
* run方法为入口

```
if __name__ == '__main__':
    shell = InstallCloud()
    shell.run()
```

### 3.2.2 节点入口

5个节点入口，对应个节点名

```
deploy_cache_node     数据采集缓存节点
deploy_database_node  数据服务数据库节点
deploy_center_node    数据中心节点
deploy_process_node   数据处理节点
deploy_storage_node   数据存储节点
deploy_bus_node       数据总线节点
```
### 3.2.3 函数入口
```
prepare                          参数配置
check                            环境检查
get_mysql_port                   服务获取mysql数据库端口
_sys_cmd_check                    命令执行结果检查
_supervisor_status_check         supervisor服务状态检查
system_ports                     获取当前服务器已用端口
system_new_port                  生成可用端口
kill_process                     杀死某端口进程
_docker_maker                    创建docker容器
_supervisor_server               创建supervisor进程
_unzip                           解压缩包
_install_env                     安装环境
_dev_ins                         环境编译
get_config                       获取指定配置项
get_all_config                   获取所有配置
set_config                       设置指定配置
replace_config                   服务替换配置项
_redis_daemon                    启动redis服务
_celery                          启动celery服务
_start_work                      启动服务指定服务
_init_server                     服务初始化
_get_docker_info                 获取docker相关配置
```
