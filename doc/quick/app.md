# 采集插件app.py文件说明


维护人 | 版本号 | 描述 | 日期
--- | --- | --- | ---
彭楚伟 | 0.1 | 初版 | 2019-07-05



本文件为采集插件主体部分，主要分三个大类：Rom，Application，Schedule；另有几个主要函数，作用为外界调度入口及配置导入
程序启动自动加载内部配置项，并由外部导入的配置项覆盖更新配置

* 脚本名称：`uwsgi_plugins/templates/{采集插件名}/app.py` ，位于对应采集插件专有分支下

实现逻辑流程

1、调用Application类获取数据

2、区分设备状态信息与报警信息，根据文档构造数据结构

3、使用_put_message函数推出数据

# 1、函数部分
## 1.1 init 函数
初始化函数
采集入口在这里实现初始化
例：
```
def init():
    CONFIG['logger.path'] = '/tmp/collect_pressure.log'
    CONFIG['collect_pressure.port'] = 8889
    CONFIG['collect_pressure.host'] = '127.0.0.1'
    _config = read_config(os.path.join(os.path.dirname(__file__), 'app.ini'))
    CONFIG.update(_config)
    if not os.path.isabs(CONFIG['app_db']):
        CONFIG['app_db'] = os.path.join(os.path.dirname(__file__), CONFIG['app_db'])

    def db_init(db_path):
        engine = create_engine('sqlite:///{}'.format(db_path))
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        return Session()

    _APP.sqlite_ses = db_init(CONFIG['app_db'])
    a = _APP.sqlite_ses.query(Refresh).filter_by().first()
    if not a:
        one = Refresh(id=1, name='KY_PRESSURE', time=(datetime.datetime.now()-datetime.timedelta(days=365)))
        _APP.sqlite_ses.add(one)
        _APP.sqlite_ses.commit()
    _APP.CONF.update(CONFIG)
```

## 1.2 config函数
更新外部配置
例：
```
def config(**global_config):
    c = dict()
    c['logger.path'] = global_config.get('logger.path', CONFIG['logger.path'])
    c['collect_pressure.logger.path'] = global_config.get('collect_pressure.logger.path', CONFIG['logger.path'])
    c['collect_pressure.host'] = global_config.get('collect_pressure.host',
                                                   CONFIG['collect_pressure.host'])
    c['collect_pressure.port'] = global_config.get('collect_pressure.port',
                                                   CONFIG['collect_pressure.port'])
    c['ky_server_username'] = global_config.get('ky_server_username',
                                                CONFIG['ky_server_username'])
    c['ky_server_password'] = global_config.get('ky_server_password',
                                                CONFIG['ky_server_password'])
    c['ky_server_api_host'] = global_config.get('ky_server_api_host',
                                                CONFIG['ky_server_api_host'])
    c['ky_server_login'] = global_config.get('ky_server_login',
                                             CONFIG['ky_server_login'])
    c['ky_server_genneral_path'] = global_config.get('ky_server_genneral_path',
                                                     CONFIG['ky_server_genneral_path'])
    c['ky_server_project_url'] = global_config.get('ky_server_project_url',
                                                   CONFIG['ky_server_project_url'])
    CONFIG.update(c)
    _APP.CONF.update(CONFIG)
    return CONFIG

```


## 1.3 read_config函数
读取配置函数
例：
```
def read_config(name, section='app:main'):
    _ret = {}
    c = ConfigParser()
    c.read(name)
    if c.has_section(section):
        for i in c.options(section):
            _ret[i] = c.get(section, i)
    return _ret
```

## 1.4 start函数
外部调度入口
* 此处内容为自动生成，一般不需要进行更改


## 1.5 _put_message函数
传出数据函数
* 此处内容为自动生成，一般不需要进行更改


## 1.6 _get_data函数
获取数据函数
* 此处内容为自动生成，一般不需要进行更改


## 1.7 _put_result函数
生成字典
* 此处内容为自动生成，一般不需要进行更改


## 1.8 system_ports和system_new_port函数
* 此处内容为自动生成，一般不需要进行更改
用于生成一个空闲的端口供服务使用，

* 调用方法：
`PORT = system_new_port()` 


# 2、 类部分
## 2.1 Rom类
负责调用rom
此处内容需要根据数据格式进行相应个性化定制修改
例：
```
class Rom(object):
    def __init__(self):
        self.rom_info = dict()

    def update(self, **device_config):
        self.rom_info = {'data': dict()}
        _d = self.rom_info['data']
        for _tid, _info in device_config['rom.config'].items():
            _info_id = _info['id']
            if _info_id not in _d:
                _d[_info_id] = dict()
                _d[_info_id]['dtype'], \
                _d[_info_id]['dmanu'], \
                _d[_info_id]['dver'] = _info['store'].split('_')
                _d[_info_id]['did'] = _info['did']
                _d[_info_id]['fields'] = dict(
                    [(i, j['defaultvalue']) for i, j in _info['fields'].items()]
                )

    def burn_message(self, info_id, name='default', version='0.1.0', status='basic'):
        if info_id not in self.rom_info['data']:
            return
        _ret = {}
        _d = self.rom_info['data'][info_id]['fields']
        for k in _d.keys():
            _ret[k] = _d[k]
        if _ret:
            _ret['_did'] = self.rom_info['data'][info_id]['did']
            _ret['_dtype'] = self.rom_info['data'][info_id]['dtype']
            _ret['_dmanu'] = self.rom_info['data'][info_id]['dmanu']
            _ret['_dver'] = self.rom_info['data'][info_id]['dver']
            _time = str(int(time.time()))
            _ret['_datetime'] = _time
            _ret['_timestamp'] = _time
            _ret['_name'] = name
            _ret['_version'] = version
            _ret['_status'] = status
        return _ret
```


## 2.2 Application类
负责采集逻辑实现
由外部调用入口start调度，输出参数为数据列表
所有采集主流程都在这里实现，并在程序脚本调用入口调用实现（来自Schedule类）
* 此处内容为改脚本插件最核心部分，也是采集逻辑实现核心代码区
* 在此类中创建方法从外部接口获取相应数据信息返回，供外部调用获取数据


## 2.3 Schedule类
主流程类，run开始任务，此处为采集入口
* 此类为调用Application类获取数据并按照要求区分状态信息和报警信息构造指定数据结构返回推送到总线
* 状态信息示例：
```
# 构造
_d = _APP.rom.burn_message(_data['deviceMac'],
                           name=self.name,
                           version=self.version,
                           status='status')
# 补充
_d['deviceType'] = _get_data(_data, 'deviceType')
                    _d['deviceMac'] = _get_data(_data, 'deviceMac')
                    _d['timeRun'] = _get_data(_data, 'timeRun')
                    _d['timeNow'] = _get_data(_data, 'timeNow')
# 推送
_put_message(_d)
```
* 报警信息示例：
```
# 构造
_d = _APP.rom.burn_message(_data['deviceMac'],
                           name=self.name,
                           version=self.version,
                           status='report')
# 补充
_d['reportType'] = _get_data(_data, 'reportType')
                _d['deviceType'] = _get_data(_data, 'deviceType')
                _d['timestamp'] = _get_data(_data, 'timestamp')
                _d['_timestamp'] = _get_data(_data, 'timestamp')
# 推送
_put_message(_d)
```

# 3、配置说明

## 3.1 电表electric
参数名 | 描述 
--- | --- 
collect_electric.host | 采集ip地址
collect_electric.port | 采集端口
collect_electric.data_url | 采集webservice接口url
collect_electric.service_api | 采集webservice接口api名


## 3.2 快越科技下部分共有配置
* 包含设备：地磁geomagnetism，温湿度humiture，门磁magnetic_door，井盖manhole_cover，压力pressure，烟感smoke，超声波ultrasonic，wifi

参数名 | 描述 
--- | --- 
logger.path | 日志存储路径
collect_{type}.host | 采集ip地址
collect_{type}.port | 采集端口
ky_server_username | 登录用户名
ky_server_password | 登录密码
ky_server_api_host | 采集接口地址
ky_server_login | 登录获取token的url
ky_server_genneral_path | 采集接口基础url
ky_server_project_url | 获取project的url



