
# 插件中心接口文档


## agent_api

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_agent_api_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* agent_api.host
* agent_api.logger.path
* agent_api.port

## collect_electric

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_electric_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_electric.data_url
* collect_electric.host
* collect_electric.logger.path
* collect_electric.port
* collect_electric._service_api

## collect_fire

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_fire_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_fire.host
* collect_fire.logger.path
* collect_fire.port

## collect_geomagnetism

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_geomagnetism_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_geomagnetism.host
* collect_geomagnetism.logger.path
* collect_geomagnetism.port

## collect_humiture

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_humiture_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_humiture.host
* collect_humiture.logger.path
* collect_humiture.port

## collect_magnetic

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_magnetic_door_0.2.0
版本 | 

外部操作 | 函数入口
:----:|:----:

### 包含参数
* logger.path

## collect_manhole

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_manhole_cover_0.2.0
版本 | 

外部操作 | 函数入口
:----:|:----:

### 包含参数
* logger.path

## collect_pressure

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_pressure_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_pressure.host
* collect_pressure.logger.path
* collect_pressure.port

## collect_smoke

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_smoke_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_smoke.host
* collect_smoke.logger.path
* collect_smoke.port

## collect_test

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_test_data_0.1.0
版本 | 

外部操作 | 函数入口
:----:|:----:

### 包含参数
* logger.path

## collect_ultrasonic

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_ultrasonic_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_ultrasonic.host
* collect_ultrasonic.logger.path
* collect_ultrasonic.port

## collect_water

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_water_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_water.data_url
* collect_water.host
* collect_water.logger.path
* collect_water.port
* collect_water._service_api
* collect_water.service_api

## collect_wifi

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_wifi_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_wifi.host
* collect_wifi.logger.path
* collect_wifi.port

## collect_wiselamp

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_collect_wiselamp_0.2.0
版本 | 0.2.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* collect_wiselamp.host
* collect_wiselamp.logger.path
* collect_wiselamp.port

## convert_collect

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_convert_collect_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
convert | convert

### 包含参数
* logger.path
* convert_collect.convert.type
* convert_collect.logger.path

## database_dbsql

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_database_dbsql_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
write | db_write

### 包含参数
* logger.path
* database_dbsql.charset
* database_dbsql.config
* database_dbsql.flexible
* database_dbsql.logger.path
* database_dbsql.url

## database_mongo

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_database_mongo_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
write | mongo_write

### 包含参数
* logger.path
* database_mongo.autocreate
* database_mongo.charset
* database_mongo.db
* database_mongo.logger.path
* database_mongo.tablename
* database_mongo.url

## database_presto

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_database_presto_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
query | query
check | check

### 包含参数
* logger.path

## flask_api

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_flask_api_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* flask_api.host
* flask_api.logger.path
* flask_api.port

## flask_kchannel

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_flask_kchannel_0.1.0
版本 | 

外部操作 | 函数入口
:----:|:----:

### 包含参数
* logger.path

## message_mqtt

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_message_mqtt_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
server_send | server_send
server_recv | server_recv
agent_send | agent_send
agent_recv | agent_recv

### 包含参数
* logger.path
* message_mqtt.agent_filter_keys
* message_mqtt.agent_ip
* message_mqtt.agent_port
* message_mqtt.channel
* message_mqtt.client_id
* message_mqtt.group
* message_mqtt.logger.path
* message_mqtt.password
* message_mqtt.server_ip
* message_mqtt.server_port
* message_mqtt.username

## middleware_alarm

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_middleware_alarm_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* middleware_alarm.excel_name
* middleware_alarm.logger.path

## middleware_status

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_middleware_status_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* middleware_status.config
* middleware_status.logger.path

## parser_csv

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_parser_csv_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
write_data | write_data

### 包含参数
* logger.path
* parser_csv.file.fields
* parser_csv.file.path
* parser_csv.file.rewrite
* parser_csv.logger.path

## parser_json

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_parser_json_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
write_config | write_config
write_data | write_data

### 包含参数
* logger.path
* parser_json.file.fields
* parser_json.file.path
* parser_json.file.path_set
* parser_json.file.rewrite
* parser_json.logger.path

## proto_collect

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_proto_collect_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
encode | encode
decode | decode

### 包含参数
* logger.path
* proto_collect.collect.version
* proto_collect.logger.path
* proto_collect.proto.version

## rom_pres

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_rom_pres_0.1.0
版本 | public_actions

外部操作 | 函数入口
:----:|:----:
{init | 

### 包含参数
* logger.path

## structure_alarm

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_structure_alarm_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* structure_alarm.logger.path
* structure_alarm.timestamp_format

## tool_rom

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_tool_rom_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
* tool_rom.device_id
* tool_rom.dmanu
* tool_rom.dtype
* tool_rom.name
* tool_rom.project
* tool_rom.register_host
* tool_rom.register_port
* tool_rom.rom_host
* tool_rom.rom_port
* tool_rom.signature
* tool_rom.standalone

## tool_spark

基础信息 | 说明 
:----:|:----:
所属分支 | plugin_tool_spark_0.1.0
版本 | 0.1.0

外部操作 | 函数入口
:----:|:----:
init | init
start | start
stop | stop

### 包含参数
* logger.path
